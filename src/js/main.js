/**
 * jQuery validation default settings
 * https://jqueryvalidation.org/jQuery.validator.setDefaults/
 */

/*
Анимация появления "waypoints"
*/
$('.appear').each(function() {

    var show = $(this);

    $(this).waypoint({
        handler: function() {
            show.addClass('show');
        },
        offset: 'bottom-in-view'
    });
})



$(function() {
    // Показываем модальное окно с формой захвата
    $('.showModal').click(function() {
        $.fancybox.open({
            src: "#popup_form",
            type: "inline"
        });
    });
});

/*
Открытыие меню
*/

jQuery(document).ready(function() {
    jQuery('.menu__burger').click(function(menu) {
        jQuery(this).toggleClass('active');
        jQuery('.menu').toggleClass('open');
        menu.preventDefault();
    });
});

/*
Показать пароль
*/

var showPass;
$(".eay").on('click', function() {
    $(this).toggleClass("show");
    if (!showPass) {
        $(this).prev('input').attr("type", "text");
        showPass = true;
    } else {
        $(this).prev('input').attr("type", "password");
        showPass = false;
    }
});


/*
Аккардион
*/

$('.accordion__itemtitle').on('click', function() {
    if ($(this).parent().hasClass('open')) {
        $(this).parent().removeClass('open');
    } else {
        $('.accordion__item').removeClass('open');
        $(this).parent().addClass('open');
    };
});

/*
Select2
*/
$(document).ready(function() {
    $('.lpc-form__select').select2();
});

// Tabs
jQuery(document).ready(function() {
    $('ul.tabs__caption').on('click', 'li:not(.tabs__linkTab_active)', function() {
        $(this)
            .addClass('tabs__linkTab_active').siblings().removeClass('tabs__linkTab_active')
            .closest('div.tabs').find('div.tabs__content').removeClass('tabs__content_active').eq($(this).index()).addClass('tabs__content_active');
    });
});

// Add Check
jQuery(document).ready(function() {
    $('.add').on('click', function() {
        $('.input-wrapper .lpc-form__numCheck').removeClass('js-vis');
        var clone = $('.input-wrapper').eq(0).clone();
        clone.appendTo('.input-container').find('.delete').removeClass('hide');
        clone.find('.lpc-form__numCheck').addClass('js-vis');
        clone.find('.lpc-input').val('');
        maskRRN();
        popupCheck();
    });
    $('.input-container').on('click', '.delete', function() {
        $(this).closest('.input-wrapper').remove();
        if ($('.input-wrapper').length === 1) {
            $('.input-wrapper').eq(0).find('.lpc-form__numCheck').addClass('js-vis');
        } else {
            $('.input-wrapper:last-child').find('.lpc-form__numCheck').addClass('js-vis');
        }
        popupCheck();
    });

    function maskRRN() {
        $('.input-wrapper input').each(function() {
            $(this).mask("9999 9999 9999 9999");
        });
    }

    // Popup Check
    function popupCheck() {
        $('.lpc-form__numCheckText').on('click', function() {
            $('.lpc-form__popupCheck').addClass('active');
        });

        $(document).mouseup(function(e) {
            var container = $(".lpc-form__popupCheck");
            if (container.has(e.target).length === 0) {
                $('.lpc-form__popupCheck').removeClass('active');
            }
        });

        $('.popupExit').on('click', function() {
            $('.lpc-form__popupCheck').removeClass('active');
        });
    }

    popupCheck();
});

// Mask Input
jQuery(document).ready(function() {
    if ($('input').hasClass('js-tel')) {
        $("#lpc-tel").mask("+7 (999) 999-99-99");
    }

    $(".js-rrn .lpc-input").mask("9999 9999 9999 9999");
});