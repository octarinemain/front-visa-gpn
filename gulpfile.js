'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    postcss = require('gulp-postcss'),
    cssnano = require('cssnano'),
    autoprefixer = require('autoprefixer'),
    cssImport = require("postcss-import"),
    mqpacker = require("css-mqpacker"),
    discardComments = require("postcss-discard-comments"),
    sprites = require('postcss-sprites'),
    mergeLonghand = require('postcss-merge-longhand'),

    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    imagemin = require('gulp-imagemin'),
    svgo = require('gulp-svgo'),
    rimraf = require('rimraf'),
    browserSync = require("browser-sync"),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    merge = require('merge-stream'),
    plumber = require('gulp-plumber'),
    newer = require('gulp-newer'),
    pug = require('gulp-pug'),
    htmlmin = require('gulp-htmlmin'),
    imageminJpegRecompress = require('imagemin-jpeg-recompress'),
    webpack = require('webpack-stream'),
    util = require('gulp-util'),
    ftp = require('vinyl-ftp'),
    reload = browserSync.reload;


var path = {
    build: {
        html: 'build/',
        js: 'build/js/',
        css: 'build/css/',
        img: 'build/img/',
        php: 'build/php/',
        fonts: 'build/fonts/'
    },
    src: {
        html: 'src/pages/*.pug',
        js: ['src/js/**/*.js', '!src/js/main.js'],
        style: 'src/style/main.scss',
        img: ['src/img/**/*.*', '!src/img/sprite/*'],
        php: 'src/php/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    watch: {
        html: 'src/**/*.pug',
        js: 'src/js/main.js',
        style: 'src/style/**/*.scss',
        img: 'src/img/**/*.*',
        php: 'src/php/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    vendorLibs: [
        "node_modules/sourcebuster/dist/sourcebuster.min.js",
        "node_modules/jquery-validation/dist/jquery.validate.js",
        "node_modules/ouibounce/build/ouibounce.min.js",
        "node_modules/jquery-countdown/dist/jquery.countdown.min.js",
        "node_modules/waypoints/lib/jquery.waypoints.min.js",
        "node_modules/waypoints/lib/shortcuts/inview.min.js"
    ],
    clean: './build'
};



var config = {
    server: {
        baseDir: "./build"
    },
    tunnel: false,
    host: 'localhost',
    port: 9000,
    logPrefix: "Rozhden",
    production: !!util.env.production
};



// ---------------------------------------
//              HTML  BUILD
// ---------------------------------------
gulp.task('html:build', function() {
    gulp.src(path.src.html)
        .pipe(plumber())
        .pipe(pug({ pretty: true }))
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({ stream: true }));
});


// ---------------------------------------
//              JS  BUILD
// ---------------------------------------
gulp.task('js:build', function() {
    gulp.src('src/js/main.js')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(config.production ? uglify() : util.noop())
        .pipe(config.production ? util.noop() : sourcemaps.write())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({ stream: true }));
});


// ---------------------------------------
//              STYLES BUILD
// ---------------------------------------
gulp.task('style:build', function() {
    var spriteOpts = {
        stylesheetPath: './build/css',
        spritePath: './build/img/',
        spritesmith: {
            padding: 2,
            algorithm: 'left-right'
        },
        filterBy: function(image) {
            if (image.url.indexOf('/sprite/') !== -1) {
                return Promise.resolve();
            } else {
                return Promise.reject();
            }
        }
    };
    var plugins = [
        cssImport(),
        autoprefixer({
            browsers: ['last 15 versions', '> 0%', 'ie 8', 'ie 7', 'ie 6']
        }),
        sprites(spriteOpts),
        mqpacker({
            sort: true
        }),
        mergeLonghand()
    ];
    var productionPlugins = [
        discardComments(),
        cssnano()
    ];

    gulp.src(path.src.style)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss(plugins))
        .pipe(config.production ? postcss(productionPlugins) : util.noop())
        .pipe(config.production ? util.noop() : sourcemaps.write())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({ stream: true }));
});

// ---------------------------------------
//              IMAGES BUILD
// ---------------------------------------
gulp.task('image:build', function() {
    gulp.src(path.src.img)
        .pipe(newer(path.build.img))
        .pipe(imagemin([
            imageminJpegRecompress()
        ], {
            progressive: true,
            svgoPlugins: [{ removeViewBox: false }],
            interlaced: true
        }))
        .pipe(svgo())
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({ stream: true }));
});



// ---------------------------------------
//              DEPLOY BUILD
// ---------------------------------------
// For deploy use:  FTP_PASS=password gulp build

gulp.task('deploy', function() {

    var conn = ftp.create({
        host: 'domain.ru',
        user: 'username',
        password: process.env.FTP_PASS,
        parallel: 10,
        log: util.log
    });
    var remoteDir = '/public_html/app';

    return gulp.src(['./build/**/*'], { base: './build/', buffer: false })
        .pipe(conn.newer(remoteDir))
        .pipe(conn.dest(remoteDir));
});




// ---- SYSTEM TASKS ---------
gulp.task('webserver', function() {
    browserSync(config);
});
gulp.task('clean', function(cb) {
    rimraf(path.clean, cb);
});
gulp.task('separateScripts', function() {
    gulp.src(path.vendorLibs)
        .pipe(plumber())
        .pipe(gulp.dest(path.build.js))
});



gulp.task('moveFiles', function() {
    gulp.src(path.src.fonts).pipe(gulp.dest(path.build.fonts));
    gulp.src(path.src.php).pipe(gulp.dest(path.build.php));
    gulp.src('src/docs/*.*').pipe(gulp.dest('build/docs/'));
    gulp.src('src/favicon.ico').pipe(gulp.dest('build/'));
    gulp.src('src/.htaccess').pipe(gulp.dest('build/'));
});
gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});
gulp.task('php:build', function() {
    gulp.src(path.src.php)
        .pipe(gulp.dest(path.build.php))
});




gulp.task('build', [
    'separateScripts',
    'moveFiles',
    'image:build',
    'html:build',
    'js:build',
    'style:build'
]);



gulp.task('watch', function() {
    watch([path.watch.html], function(event, cb) { gulp.start('html:build'); });
    watch([path.watch.style], function(event, cb) { gulp.start('style:build'); });
    watch([path.watch.js], function(event, cb) { gulp.start('js:build'); });
    watch([path.watch.img], function(event, cb) { gulp.start('image:build'); });
    watch([path.watch.fonts], function(event, cb) { gulp.start('fonts:build'); });
    watch([path.watch.php], function(event, cb) { gulp.start('php:build'); });
});



gulp.task('default', ['build', 'webserver', 'watch']);