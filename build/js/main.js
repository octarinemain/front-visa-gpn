/**
 * jQuery validation default settings
 * https://jqueryvalidation.org/jQuery.validator.setDefaults/
 */

/*
Анимация появления "waypoints"
*/
$('.appear').each(function() {

    var show = $(this);

    $(this).waypoint({
        handler: function() {
            show.addClass('show');
        },
        offset: 'bottom-in-view'
    });
})



$(function() {
    // Показываем модальное окно с формой захвата
    $('.showModal').click(function() {
        $.fancybox.open({
            src: "#popup_form",
            type: "inline"
        });
    });
});

/*
Открытыие меню
*/

jQuery(document).ready(function() {
    jQuery('.menu__burger').click(function(menu) {
        jQuery(this).toggleClass('active');
        jQuery('.menu').toggleClass('open');
        menu.preventDefault();
    });
});

/*
Показать пароль
*/

var showPass;
$(".eay").on('click', function() {
    $(this).toggleClass("show");
    if (!showPass) {
        $(this).prev('input').attr("type", "text");
        showPass = true;
    } else {
        $(this).prev('input').attr("type", "password");
        showPass = false;
    }
});


/*
Аккардион
*/

$('.accordion__itemtitle').on('click', function() {
    if ($(this).parent().hasClass('open')) {
        $(this).parent().removeClass('open');
    } else {
        $('.accordion__item').removeClass('open');
        $(this).parent().addClass('open');
    };
});

/*
Select2
*/
$(document).ready(function() {
    $('.lpc-form__select').select2();
});

// Tabs
jQuery(document).ready(function() {
    $('ul.tabs__caption').on('click', 'li:not(.tabs__linkTab_active)', function() {
        $(this)
            .addClass('tabs__linkTab_active').siblings().removeClass('tabs__linkTab_active')
            .closest('div.tabs').find('div.tabs__content').removeClass('tabs__content_active').eq($(this).index()).addClass('tabs__content_active');
    });
});

// Add Check
jQuery(document).ready(function() {
    $('.add').on('click', function() {
        $('.input-wrapper .lpc-form__numCheck').removeClass('js-vis');
        var clone = $('.input-wrapper').eq(0).clone();
        clone.appendTo('.input-container').find('.delete').removeClass('hide');
        clone.find('.lpc-form__numCheck').addClass('js-vis');
        clone.find('.lpc-input').val('');
        maskRRN();
        popupCheck();
    });
    $('.input-container').on('click', '.delete', function() {
        $(this).closest('.input-wrapper').remove();
        if ($('.input-wrapper').length === 1) {
            $('.input-wrapper').eq(0).find('.lpc-form__numCheck').addClass('js-vis');
        } else {
            $('.input-wrapper:last-child').find('.lpc-form__numCheck').addClass('js-vis');
        }
        popupCheck();
    });

    function maskRRN() {
        $('.input-wrapper input').each(function() {
            $(this).mask("9999 9999 9999 9999");
        });
    }

    // Popup Check
    function popupCheck() {
        $('.lpc-form__numCheckText').on('click', function() {
            $('.lpc-form__popupCheck').addClass('active');
        });

        $(document).mouseup(function(e) {
            var container = $(".lpc-form__popupCheck");
            if (container.has(e.target).length === 0) {
                $('.lpc-form__popupCheck').removeClass('active');
            }
        });

        $('.popupExit').on('click', function() {
            $('.lpc-form__popupCheck').removeClass('active');
        });
    }

    popupCheck();
});

// Mask Input
jQuery(document).ready(function() {
    if ($('input').hasClass('js-tel')) {
        $("#lpc-tel").mask("+7 (999) 999-99-99");
    }

    $(".js-rrn .lpc-input").mask("9999 9999 9999 9999");
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYWluLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxyXG4gKiBqUXVlcnkgdmFsaWRhdGlvbiBkZWZhdWx0IHNldHRpbmdzXHJcbiAqIGh0dHBzOi8vanF1ZXJ5dmFsaWRhdGlvbi5vcmcvalF1ZXJ5LnZhbGlkYXRvci5zZXREZWZhdWx0cy9cclxuICovXHJcblxyXG4vKlxyXG7QkNC90LjQvNCw0YbQuNGPINC/0L7Rj9Cy0LvQtdC90LjRjyBcIndheXBvaW50c1wiXHJcbiovXHJcbiQoJy5hcHBlYXInKS5lYWNoKGZ1bmN0aW9uKCkge1xyXG5cclxuICAgIHZhciBzaG93ID0gJCh0aGlzKTtcclxuXHJcbiAgICAkKHRoaXMpLndheXBvaW50KHtcclxuICAgICAgICBoYW5kbGVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgc2hvdy5hZGRDbGFzcygnc2hvdycpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgb2Zmc2V0OiAnYm90dG9tLWluLXZpZXcnXHJcbiAgICB9KTtcclxufSlcclxuXHJcblxyXG5cclxuJChmdW5jdGlvbigpIHtcclxuICAgIC8vINCf0L7QutCw0LfRi9Cy0LDQtdC8INC80L7QtNCw0LvRjNC90L7QtSDQvtC60L3QviDRgSDRhNC+0YDQvNC+0Lkg0LfQsNGF0LLQsNGC0LBcclxuICAgICQoJy5zaG93TW9kYWwnKS5jbGljayhmdW5jdGlvbigpIHtcclxuICAgICAgICAkLmZhbmN5Ym94Lm9wZW4oe1xyXG4gICAgICAgICAgICBzcmM6IFwiI3BvcHVwX2Zvcm1cIixcclxuICAgICAgICAgICAgdHlwZTogXCJpbmxpbmVcIlxyXG4gICAgICAgIH0pO1xyXG4gICAgfSk7XHJcbn0pO1xyXG5cclxuLypcclxu0J7RgtC60YDRi9GC0YvQuNC1INC80LXQvdGOXHJcbiovXHJcblxyXG5qUXVlcnkoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xyXG4gICAgalF1ZXJ5KCcubWVudV9fYnVyZ2VyJykuY2xpY2soZnVuY3Rpb24obWVudSkge1xyXG4gICAgICAgIGpRdWVyeSh0aGlzKS50b2dnbGVDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgICAgalF1ZXJ5KCcubWVudScpLnRvZ2dsZUNsYXNzKCdvcGVuJyk7XHJcbiAgICAgICAgbWVudS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgfSk7XHJcbn0pO1xyXG5cclxuLypcclxu0J/QvtC60LDQt9Cw0YLRjCDQv9Cw0YDQvtC70YxcclxuKi9cclxuXHJcbnZhciBzaG93UGFzcztcclxuJChcIi5lYXlcIikub24oJ2NsaWNrJywgZnVuY3Rpb24oKSB7XHJcbiAgICAkKHRoaXMpLnRvZ2dsZUNsYXNzKFwic2hvd1wiKTtcclxuICAgIGlmICghc2hvd1Bhc3MpIHtcclxuICAgICAgICAkKHRoaXMpLnByZXYoJ2lucHV0JykuYXR0cihcInR5cGVcIiwgXCJ0ZXh0XCIpO1xyXG4gICAgICAgIHNob3dQYXNzID0gdHJ1ZTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgJCh0aGlzKS5wcmV2KCdpbnB1dCcpLmF0dHIoXCJ0eXBlXCIsIFwicGFzc3dvcmRcIik7XHJcbiAgICAgICAgc2hvd1Bhc3MgPSBmYWxzZTtcclxuICAgIH1cclxufSk7XHJcblxyXG5cclxuLypcclxu0JDQutC60LDRgNC00LjQvtC9XHJcbiovXHJcblxyXG4kKCcuYWNjb3JkaW9uX19pdGVtdGl0bGUnKS5vbignY2xpY2snLCBmdW5jdGlvbigpIHtcclxuICAgIGlmICgkKHRoaXMpLnBhcmVudCgpLmhhc0NsYXNzKCdvcGVuJykpIHtcclxuICAgICAgICAkKHRoaXMpLnBhcmVudCgpLnJlbW92ZUNsYXNzKCdvcGVuJyk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgICQoJy5hY2NvcmRpb25fX2l0ZW0nKS5yZW1vdmVDbGFzcygnb3BlbicpO1xyXG4gICAgICAgICQodGhpcykucGFyZW50KCkuYWRkQ2xhc3MoJ29wZW4nKTtcclxuICAgIH07XHJcbn0pO1xyXG5cclxuLypcclxuU2VsZWN0MlxyXG4qL1xyXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcclxuICAgICQoJy5scGMtZm9ybV9fc2VsZWN0Jykuc2VsZWN0MigpO1xyXG59KTtcclxuXHJcbi8vIFRhYnNcclxualF1ZXJ5KGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcclxuICAgICQoJ3VsLnRhYnNfX2NhcHRpb24nKS5vbignY2xpY2snLCAnbGk6bm90KC50YWJzX19saW5rVGFiX2FjdGl2ZSknLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAkKHRoaXMpXHJcbiAgICAgICAgICAgIC5hZGRDbGFzcygndGFic19fbGlua1RhYl9hY3RpdmUnKS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKCd0YWJzX19saW5rVGFiX2FjdGl2ZScpXHJcbiAgICAgICAgICAgIC5jbG9zZXN0KCdkaXYudGFicycpLmZpbmQoJ2Rpdi50YWJzX19jb250ZW50JykucmVtb3ZlQ2xhc3MoJ3RhYnNfX2NvbnRlbnRfYWN0aXZlJykuZXEoJCh0aGlzKS5pbmRleCgpKS5hZGRDbGFzcygndGFic19fY29udGVudF9hY3RpdmUnKTtcclxuICAgIH0pO1xyXG59KTtcclxuXHJcbi8vIEFkZCBDaGVja1xyXG5qUXVlcnkoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xyXG4gICAgJCgnLmFkZCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICQoJy5pbnB1dC13cmFwcGVyIC5scGMtZm9ybV9fbnVtQ2hlY2snKS5yZW1vdmVDbGFzcygnanMtdmlzJyk7XHJcbiAgICAgICAgdmFyIGNsb25lID0gJCgnLmlucHV0LXdyYXBwZXInKS5lcSgwKS5jbG9uZSgpO1xyXG4gICAgICAgIGNsb25lLmFwcGVuZFRvKCcuaW5wdXQtY29udGFpbmVyJykuZmluZCgnLmRlbGV0ZScpLnJlbW92ZUNsYXNzKCdoaWRlJyk7XHJcbiAgICAgICAgY2xvbmUuZmluZCgnLmxwYy1mb3JtX19udW1DaGVjaycpLmFkZENsYXNzKCdqcy12aXMnKTtcclxuICAgICAgICBjbG9uZS5maW5kKCcubHBjLWlucHV0JykudmFsKCcnKTtcclxuICAgICAgICBtYXNrUlJOKCk7XHJcbiAgICAgICAgcG9wdXBDaGVjaygpO1xyXG4gICAgfSk7XHJcbiAgICAkKCcuaW5wdXQtY29udGFpbmVyJykub24oJ2NsaWNrJywgJy5kZWxldGUnLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAkKHRoaXMpLmNsb3Nlc3QoJy5pbnB1dC13cmFwcGVyJykucmVtb3ZlKCk7XHJcbiAgICAgICAgaWYgKCQoJy5pbnB1dC13cmFwcGVyJykubGVuZ3RoID09PSAxKSB7XHJcbiAgICAgICAgICAgICQoJy5pbnB1dC13cmFwcGVyJykuZXEoMCkuZmluZCgnLmxwYy1mb3JtX19udW1DaGVjaycpLmFkZENsYXNzKCdqcy12aXMnKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAkKCcuaW5wdXQtd3JhcHBlcjpsYXN0LWNoaWxkJykuZmluZCgnLmxwYy1mb3JtX19udW1DaGVjaycpLmFkZENsYXNzKCdqcy12aXMnKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcG9wdXBDaGVjaygpO1xyXG4gICAgfSk7XHJcblxyXG4gICAgZnVuY3Rpb24gbWFza1JSTigpIHtcclxuICAgICAgICAkKCcuaW5wdXQtd3JhcHBlciBpbnB1dCcpLmVhY2goZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICQodGhpcykubWFzayhcIjk5OTkgOTk5OSA5OTk5IDk5OTlcIik7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gUG9wdXAgQ2hlY2tcclxuICAgIGZ1bmN0aW9uIHBvcHVwQ2hlY2soKSB7XHJcbiAgICAgICAgJCgnLmxwYy1mb3JtX19udW1DaGVja1RleHQnKS5vbignY2xpY2snLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgJCgnLmxwYy1mb3JtX19wb3B1cENoZWNrJykuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAkKGRvY3VtZW50KS5tb3VzZXVwKGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICAgICAgdmFyIGNvbnRhaW5lciA9ICQoXCIubHBjLWZvcm1fX3BvcHVwQ2hlY2tcIik7XHJcbiAgICAgICAgICAgIGlmIChjb250YWluZXIuaGFzKGUudGFyZ2V0KS5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgICAgICQoJy5scGMtZm9ybV9fcG9wdXBDaGVjaycpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAkKCcucG9wdXBFeGl0Jykub24oJ2NsaWNrJywgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICQoJy5scGMtZm9ybV9fcG9wdXBDaGVjaycpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwb3B1cENoZWNrKCk7XHJcbn0pO1xyXG5cclxuLy8gTWFzayBJbnB1dFxyXG5qUXVlcnkoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xyXG4gICAgaWYgKCQoJ2lucHV0JykuaGFzQ2xhc3MoJ2pzLXRlbCcpKSB7XHJcbiAgICAgICAgJChcIiNscGMtdGVsXCIpLm1hc2soXCIrNyAoOTk5KSA5OTktOTktOTlcIik7XHJcbiAgICB9XHJcblxyXG4gICAgJChcIi5qcy1ycm4gLmxwYy1pbnB1dFwiKS5tYXNrKFwiOTk5OSA5OTk5IDk5OTkgOTk5OVwiKTtcclxufSk7Il0sImZpbGUiOiJtYWluLmpzIiwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
